#!/usr/bin/env python
from gssapi import Credentials
import paramiko
from igwn_wave_compare import configs
from pesummary.gw.file.read import read
import os
import sys
import ast
import numpy as np 
try:
    import configparser
except ImportError: 
    # python < 3
    import ConfigParser as configparser
import time
import argparse
import re

def peParse(opts):
    if opts.pe_cfile_path:
        pe_config = configparser.ConfigParser()
        pe_config.optionxform = str
        try:
            cf = pe_config.read(opts.pe_cfile_path)
        except:
            print('IWC config not readable by ConfigParser. Please try again.')
            sys.exit(1)
        return cf
    elif opts.pesummfile_path:
        try:
            pe_dat = read(opts.pesummfile_path)
        except:
            print('Error: pesummaryfile could not be read. Please try again')
            sys.exit(1)  
        if opts.pesumm_o3b:
            return pe_dat.config[opts.pesummarylabel]['config']
        elif opts.pesumm_o4a:
            return pe_dat.config['config']

def receive_output(channel, timeout=2):
    """Receive data from the channel until a timeout occurs."""
    output = ''
    start_time = time.time()
    while True:
        if channel.recv_ready():
            output += channel.recv(1024).decode('utf-8')
            start_time = time.time()  # Reset the timer when data is received
        else:
            time.sleep(0.1)
            # Break the loop if no more data after the timeout duration
            if time.time() - start_time > timeout:
                break
    return output   
        

parser = argparse.ArgumentParser(description=__doc__,
                                formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("--pesummfile-path", type=str, help="Path to PE file.", )
parser.add_argument("--iwc-config-path", type=str, help="Path to IWC config file.", )
parser.add_argument("--pesumm-o4a", default=False, action="store_true", help="Use pesummary file, O4a (pass with --pesummfile-path).")
parser.add_argument("--pesumm-o3b", default=False, action="store_true", help="Use pesummary file, O3b (pass with --pesummfile-path).")
parser.add_argument("--pe-cfile-path", type=str, help="Path to standalone PE config file")
parser.add_argument("--pesummarylabel", type=str, help="PE summary label for O3b production pesummaryfile")
parser.add_argument("--event-name", type=str, required=True, help="Name of event.", )
parser.add_argument("--output-path", type=str, default='./', help="Output Path.", )

opts = parser.parse_args()

if opts.output_path == './':
    output_path = os.getcwd()
    if not os.access(output_path, os.W_OK):
        print(f"Error: The directory {output_path} is not writable.")
        sys.exit(1)
else:
    output_path = opts.output_path
    if not os.path.exists(output_path):
        print(f"Warning: The directory {output_path} does not exist. Making directory")
        # Optionally, you can create the directory here if it doesn't exist
        os.makedirs(output_path)
    elif not os.access(output_path, os.W_OK):
        print(f"Error: The directory {output_path} is not writable.")
        sys.exit(1)
    
opts = parser.parse_args()    
    
### opts validity checking
if not opts.iwc_config_path:
    print('Error: --iwc-config-path  not passed. Try again.')
    sys.exit(1)

    
if opts.pesummfile_path:
    # try:
    #     with read(opts.pesummfile_path) as fp:
    #         print(f'PE Summary file {opts.iwc_config_path} file readable.')

    # except IOError as err:
    #     print(f'Error: PE Summary file file not readable or does not exist.')
    #     sys.exit(1)
    
    if not (opts.pesumm_o3b or opts.pesumm_o4a):
        print('Error: --pesummfile-path was passed and NOT one of --pe-summ-o3b or --pe-summ-o4a passed. Try again.')
        sys.exit(1)
    elif opts.pesumm_o3b and opts.pesumm_o4a:
        print('Error: --pesumm-o3b and --pesumm-o4a passed. Choose one or the other.')
        sys.exit(1) 
    elif opts.pesumm_o3b and not opts.pesummarylabel:
        print('Error: --pe-summ-o3b passed without --pesummarylabel. Try again.')
        sys.exit(1)
        
elif opts.pe_cfile_path:
    if opts.pesummfile_path:
        print('Error: --pe-cfile-path and --pesummfile-path were both passed. Choose one or the other, and try again.')
        sys.exit(1)
    try:
        with open(opts.pe_cfile_path) as fp:
            print(f'PE Config file {opts.iwc_config_path} file readable.')

    except IOError as err:
        print(f'Error: PE Config file file not readable or does not exist.')
        sys.exit(1)
else:
    print('Error: One of --pe-cfile-path or --pesummfile-path not passed. Pass a valid PE summary file or PE config file and try again.')   
    sys.exit(1)
    
### check to see if sourced o4a-pyEnv.sh
if os.getenv('IWC_REPO') == None:
    print('Error: Must source o4a-pyEnv.sh in your repository directory. Please try again')
    sys.exit(1)

try:
    with open(opts.iwc_config_path) as fp:
        fp.close()

except IOError as err:
    print(f'Error: IWC config file file not readable or does not exist.')
    sys.exit(1)



pe_config = peParse(opts)
try:
    event_trigtime = float(pe_config["trigger-time"])
except:
    event_trigtime = float(pe_config["trigger_time"])

if opts.pesumm_o3b:
    pe_ifos = sorted(ast.literal_eval(pe_config["analysis"]["ifos"])) ### ### MARK DEV: Read 
else:
    pe_ifos = [ifo.replace("'", "") for ifo in pe_config['detectors']]

### Parse IWC config file
iwc_config = configparser.ConfigParser()

# iwc_config.optionxform = str
try:
    iwc_config.read(opts.iwc_config_path)
except:
    print('Error: IWC config not readable by ConfigParser. Please try again.')
    sys.exit(1)
 
try:
    offs_window = int(iwc_config.get('iwc_options','offs_window'))
except:
    print('Error: offs_window not readable from IWC config file. Amend it, and try again.')
    sys.exit(1)

### must source o4a-sources.sh
iwc_repoDir = os.getenv('IWC_REPO')
boundBeg = event_trigtime - offs_window
boundEnd = event_trigtime + offs_window
# bounds = [boundBeg, boundEnd]

### running and output path at detector sites; hard-coded for now
segsPath = os.path.expanduser('~/hveto-eventSegs-O4')
local_script_path = os.path.join(iwc_repoDir, 'iwc_pipe/hveto_segs/readSegsDet.py')
remote_script_path = os.path.join(segsPath, 'readSegsDet.py')


### COMMAND: make segsPath if not exist 

# Establish SSH connection
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
dirCmd = f'[ -d {segsPath} ] || mkdir -p {segsPath}'

### SUB USER
roundTrig = int(np.floor(event_trigtime))
for ifo in pe_ifos:
    if ifo == 'H1':
        ssh.connect('ldas-grid.ligo-wa.caltech.edu', username='johnmichael.sullivan', gss_auth=True, gss_kex=True, gss_deleg_creds=True, gss_host='ldas-grid.ligo-wa.caltech.edu', gss_trust_dns=True, key_filename=False)
    elif ifo == 'L1':
        ssh.connect('ldas-grid.ligo-la.caltech.edu', username='johnmichael.sullivan', gss_auth=True, gss_kex=True, gss_deleg_creds=True, gss_host='ldas-grid.ligo-la.caltech.edu', gss_trust_dns=True, key_filename=False)
    else:
        continue
    
    ### make output directory on remote if doesn't exist 
    ssh.exec_command(dirCmd)
    
    ### transfer script to remote
    sftp = ssh.open_sftp()
    sftp.put(local_script_path, remote_script_path)
    sftp.close()
    
    outName = f'{ifo}_HVETO_SEGS_{opts.event_name}_{roundTrig}_{offs_window}s.txt'
    
    ### run hveto segment gathering; output path is hardcoded on remote
    # hvetoCmd = f'''conda activate igwn || true; python {remote_script_path} --beg-secs {boundBeg} --end-secs {boundEnd} --output-file-name {outName}'''
    
    ### UPDATED
    # Start an interactive shell session
    print(f'Beginning Hveto Segment Mining for {ifo}\n----------------------------------------------------------')

    channel = ssh.invoke_shell()

    # Activate conda environment
    channel.send('conda activate igwn\n')
    # Wait for the activation to finish and read its output
    activation_output = receive_output(channel, timeout=5)
    print(activation_output)  # You can choose to log this or not

    # Execute the hveto command
    hvetoCmd = f'''python {remote_script_path} --beg-secs {boundBeg} --end-secs {boundEnd} --output-file-name {outName}\n'''
    channel.send(hvetoCmd)

    # Wait for the hveto command to finish and read its output
    hv_output = receive_output(channel, timeout=16)

    # Handle the output as necessary
    print(hv_output)
    
    ### get exit status code of last (hveto) command
    # channel.send('echo $?\n')
    # exit_status_output = receive_output(channel, timeout=4)
    # # The last line of the exit_status_output should be our exit status; we strip and convert it to an integer
    # exit_status = int(exit_status_output.strip().split("\n")[-1])

    # # Close the channel
    # channel.close()


    # channel.send('echo $?')
    # exit_status_output = receive_output(channel, timeout=5)#.split("\n")
    # print(exit_status_output)
    # # Extract exit status
    # try:
    #     # Join the lines to search through them as one continuous string
    #     # joined_output = "\n".join(exit_status_output)
    #     # Search for a pattern that matches "echo $?" followed by a newline, and then one or more digits
    #     match = re.search(r'echo \$\?\n(\d+)', exit_status_output)
    #     if match:
    #         exit_status = int(match.group(1))
    #     else:
    #         raise ValueError("Pattern not found in output.")
    # except (ValueError, IndexError):
    #     print("Error: Failed to retrieve exit status.")
    #     print(exit_status_output)  # print the full output for context
    #     raise  # re-raise the exception to halt the script


    channel.close()

    print(f"Command Output: \n{hv_output}")
    # print(f"Exit Status: {exit_status}")

    
    # stdin, stdout, stderr = ssh.exec_command(hvetoCmd)
    # hv_output = stdout.read().decode('utf-8')
    # hv_error = stderr.read().decode('utf-8')
    # exit_status = stdout.channel.recv_exit_status() # Get the return code
    
    
    if 'EXIT0' in hv_output:
        print('----------------------------------------------------------')
        print(f"Hveto segment gathering command executed successfully at {ifo}!")
        # print(f"Output:\n{hv_output}"
    else:
        print('----------------------------------------------------------')
        print(f"Error: Hveto segment gathering command failed at {ifo} with return code {exit_status}")
        # print(f"Error message:\n{hv_error}")
        sys.exit(1)
        
    sftp = ssh.open_sftp()
    sftp.get(os.path.join(segsPath, outName), os.path.join(output_path, outName))
    sftp.close()
    
    ssh.close()
