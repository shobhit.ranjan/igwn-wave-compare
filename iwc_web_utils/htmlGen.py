import markdown as md
import pandas as pd


"""
Modularized HTML construction, implementing the Materialize.css library, for Ligo Scientific Collaboration.
Copyright Jack Sullivan, 2021, all rights reserved

See construct-webpage for overall mechanics of this script.
"""

# write header:


def head(event_name):
    return f"""
  <!DOCTYPE html>
  <!-- saved from url=(0077)https://ldas-jobs.ligo.caltech.edu/~megan.arogeti/OutputTest/output_test.html -->
  <html lang="en">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://ldas-jobs.ligo.caltech.edu/favicon.ico">

    <title>Igwn Wave Compare Results Summary<br>{event_name}</title>

      <!-- Compiled and minified CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    

      <!-- Compiled and minified JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
      
      <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
      
      <link href="master.css" rel="stylesheet" type="text/css" >      
     
  </head>

  <body data-new-gr-c-s-check-loaded="14.1014.0" data-gr-ext-installed="">
  """


# SCRIPT HERE ABOVE BODY!
# NEED ALSO: small text element counting
def footer():
    return """

    <script>
  
    </script>
	</body>
  
	</html>
	"""


def container(className, child):
    return """
  <div class="container %s">%s</div>
  """ % (className, child)


def row(child):
    return """
      <div class="row">%s</div>
    """ % child


def col(child, width):
    return """
    <div class="col s%s">%s</div>
    """ % (str(width), child)


def varCol(child, mobPrefix, mobWidth, prefix, width):
    return """
    <div class="col %s%s %s%s">%s</div>
    """ % (mobPrefix, str(mobWidth), prefix, str(width), child)

# make all section contents first
# call each section num at same time as make div


def sectiondiv(sectName, contents):
    idName = sectName.replace(" ", "").lower()
    return """
  <div id="%s" class="col s12 sec">%s</div>
  """ % (idName, contents)
# call each section num at same time as make div


def navItem(sectName, isFirst):
    # concat all of these -> insert as child of navBones; to put at top of html body

    # if flag ->
      # generate this item, add to respective string
      # generate div
    # hold section contents
          # work inside out (each section)
          # then combine  nav + contents at top level, when ready to put the page together

    idName = sectName.replace(" ", "").lower()
    if (isFirst == True):
        return """
        <li class="tab"><a class="active" href="#%s">%s</a></li>
      """ % (idName, sectName)
    else:
        return """
    <li class="tab"><a href="#%s">%s</a></li>
    """ % (idName, sectName)


def navBones(child, event_name):
    return f"""
    <div class="navbar-fixed">
    <nav class="nav-extended">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo center"><i class="material-icons left bubs">bubble_chart</i>Igwn Wave Compare Summary Page</a>
    </div>
    <div class="event-name center">{event_name}</div>
    <div class="nav-content"><ul class="tabs tabs-transparent">%s</ul></div>
    </nav>
    </div>
  """ % child


def h1(child, flowText):
    if flowText == True:
        return """
      <h1 class="flow-text">%s</h1>
      """ % child
    else:
        return """
    <h1> %s</h1>
    """ % child


def h2(child, flowText):
    if flowText == True:
        return """
      <h2 class="flow-text">%s</h2>
      """ % child
    else:
        return """
    <h2> %s</h2>
    """ % child

def h3(child, flowText):
    if flowText == True:
        return """
      <h3 class="flow-text">%s</h3>
      """ % child
    else:
        return """
    <h3> %s</h3>
    """ % child


def img(source):
    return container("img", row(col("""
<img class="responsive-img" src="%s">
""", 12))) % source
# Meed take out responsive image?


def imgGridVar(srcArr):
    imgGridStr = ""
    if len(srcArr) == 1:
        imgGridStr += varCol(img(srcArr[i]), "s", 12, "m", 12)
        return container("img", row(imgGridStr))

    for i in range(len(srcArr)):
        if srcArr[i]:  # exists
            imgGridStr += varCol(img(srcArr[i]), "s", 12, "m", 6)
    return container("img", row(imgGridStr))


def linkButton(source):
  # might need addlt. div here
    if ("offsource_injections" in source):
      datFile = open(source, 'r', encoding='utf-8')
      readed = str(len(datFile.read().split('\n'))-1)
      infoStr = '%s overlap points / 200 found'%readed
      return """<a href="%s" class="download waves-effect waves-light btn-large" download><i class="material-icons left">attach_file</i> Download Data</a><h2 class="infoHold">%s</h2>""" % (source, infoStr)
    
    elif("offsource_background" in source):
      datFile = open(source, 'r', encoding='utf-8')
      readed = str(len(datFile.read().split('\n'))-1)
      infoStr = '%s offsource snr 90 points / 200 found'%readed
      return """<a href="%s" class="download waves-effect waves-light btn-large" download><i class="material-icons left">attach_file</i> Download Data</a><h2 class="infoHold">%s</h2>""" % (source, infoStr)
    else: 
      return """<a href="%s" class="download waves-effect waves-light btn-large" download><i class="material-icons left">attach_file</i> Download Data</a>""" %source


def collapsible():
    return """
     <ul class="collapsible">

    </ul>
    """

def collapsibleItem(child, title):
    return """
    <li>
      <div class="collapsible-header">%s</div>
      <div class="collapsible-body"><span>%s</span></div>
    </li>
    """ % (title, child)

def table(markdownPath):
    f = open(markdownPath, 'r')
    return md.markdown(f.read(), extensions=['tables'])
