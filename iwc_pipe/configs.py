from __future__ import print_function

import argparse
import ast
import copy
import gzip
import os
import shutil
import subprocess
import sys
import pandas as pd
import pickle 
import math
from igwn_wave_compare import io
from ligo.lw.lsctables import SimInspiralTable

from pesummary.gw.file.read import read

try:
    import configparser
except ImportError:  # python < 3
    import ConfigParser as configparser

import numpy as np
from glue import pipeline

from ligo import segments
from ligo.segments.utils import tosegwizard

from iwc_pipe import bayeswave_pipe_utils as pipe_utils
from iwc_pipe import iwc_pipe_utils as iwc_utils


try:
    import configparser
except ImportError:  # python < 3
    import ConfigParser as configparser


### HTCondor Constructors
'''
Here, improving setting of flags, etc; make transferrable
'''
       
frame_to_channel_dictionary = {
    'DCS-CALIB_STRAIN_CLEAN_SUB60HZ_C01': 'HOFT_CLEAN_SUB60HZ_C01',
    'DCS-CALIB_STRAIN_CLEAN_C01': 'HOFT_C01',
    'GDS-CALIB_STRAIN_CLEAN': 'HOFT_C00',
    'Hrec_hoft_16384Hz': 'Online'

}

def load(path):
    f = open(path, 'rb' )
    return pickle.load(f)

def save(obj, path):
    f = open(path, 'wb' )
    pickle.dump(obj,f)
    return

def confirm(prompt=None, resp=False):
    """Lifted from:
    http://code.activestate.com/recipes/541096-prompt-the-user-for-confirmation/
    Prompts for yes or no response from the user. Returns True for yes and
    False for no.

    'resp' should be set to the default value assumed by the caller when
    user simply types ENTER.

    >>> confirm(prompt='Proceed?', resp=True)
    Create Directory? [y]|n: 
    True
    >>> confirm(prompt='Proceed?', resp=False)
    Create Directory? [n]|y: 
    False
    >>> confirm(prompt='Proceed?', resp=False)
    Create Directory? [n]|y: y
    True

    """

    if prompt is None:
        prompt = 'Confirm'

    if resp:
        prompt = '%s [%s]|%s: ' % (prompt, 'y', 'n')
    else:
        prompt = '%s [%s]|%s: ' % (prompt, 'n', 'y')

    while True:
        ans = input(prompt)
        if not ans:
            return resp
        if ans not in ['y', 'Y', 'n', 'N']:
            print('please enter y or n.')
            continue
        if ans in ['y', 'Y']:
            return True
        if ans in ['n', 'N']:
            return False     

def infer_frame_types(iwc_config, channel_type_dict, glitch_clean_suffix='_T1700406_v4'):
    '''
    Poss types of frames from gw_data_find (Updated: O4)
    H1_HOFT_CLEAN_SUB60HZ_C01_T1700406_v4
    L1_HOFT_CLEAN_SUB60HZ_C01_T1700406_v4
    L1_HOFT_C01_T1700406_v4
    L1_HOFT_C01_T1700406_v4_T1700406_v4
    H1_HOFT_C01_T1700406_v4
    V1Online_T1700406_v4
    '''
    if iwc_config.has_option('iwc_options', 'frtype'):
        frame_type_dict = ast.literal_eval(iwc_config.get('iwc_options', 'frtype'))
        return frame_type_dict
    else:
        try:
            frame_type_dict = {}
            for key, chan in channel_type_dict.items():
                
                
                chan_suffix = chan.split(':')[1]
            
                # chan_suffix = chan

                # Count number of time glitch cleaned suffix apears in channel name
                clean_count = chan_suffix.count(glitch_clean_suffix)
                
                ### take out known glitch clean suffix; if we pass glitch cleaned frames
                if clean_count > 0:
                    chan_suffix = chan_suffix.replace(glitch_clean_suffix, '')

                frame_suffix = frame_to_channel_dictionary[chan_suffix]
                ### frame type name 
                frame_name = f'V1{frame_suffix}' if key == 'V1' else f'{key}_{frame_suffix}'
                ### add back glitch clean suffix
                if clean_count > 0:
                    for _ in range(clean_count):
                        frame_name = frame_name + glitch_clean_suffix
                frame_type_dict[key] = frame_name
            return frame_type_dict
        except:
            print('Error: frame types could not be determined from channels. Consider passing in frame types in iwc_config as frtype under iwc_options.')
            sys.exit(1)

    
class ConfigParse(object):
    
    @staticmethod
    def peDictClean(dict_str):
            pe_dict = {
                key.strip().strip("'"): value.strip().strip("'") for key, value in (
                    item.split(':') for item in dict_str.strip('{}').split(',') if ':' in item  # Filter out items without a colon
                )
            }
            
            return pe_dict
            ### need to check for len of psd 
            
    def __init__(self):
        
        def parser():
            """
            Parser for iwc_pipe (command line and ini file)
            """
            parser = argparse.ArgumentParser(description=__doc__,
                                            formatter_class=argparse.RawDescriptionHelpFormatter)

            parser.add_argument("configfile", help="igwn-wave-compare config file", )
            parser.add_argument("--pesummaryfile", help="PE summary file to use for recostructions and residuals analysis")
            # parser.add_argument("--pedatfile", default=False, action="store_true", help='Use individual PE files (PSD, pesamples, peconfig) files in O3b format.')
            parser.add_argument("--bilby-o3b", default=False, action="store_true", help='Use O3b-era Bilby PE Summary Fiiles.')
            parser.add_argument("--bilby-o4a", default=False, action="store_true", help='Use O4a-era Bilby PE Summary Fiiles.')

            parser.add_argument("--peconfigfile", default=None,
                                help="PE config file. (This option exists for cases where there's no PEsummaryfile and the "
                                    "input PE data is a combination of old-style (Bilby/LI) posterior samples, a config file, and PSD "
                                    "files)")
            parser.add_argument("--psd-files", default=None, help="PSD files. Provide PSDs. (This option exists for cases where there's no PEsummaryfile and the "
                                    "input PE data is a combination of old-style (Bilby/LI) posterior samples, a config file, and PSD "
                                    "files)", nargs='*')
            parser.add_argument("--pesamplesfile", default=None, help="PE Posterior Samples File. (This option exists for cases where there's no PEsummaryfile and the "
                                    "input PE data is a combination of old-style (Bilby/LI) posterior samples, a config file, and PSD "
                                    "files)")    
            
            parser.add_argument("-l", "--pesummarylabel", default=None,
                                help="Label of the CBC. If not provide, I will print list of "
                                    "available labels and exit")
            parser.add_argument("-r", "--workdir", type=str, default=None)
            parser.add_argument("--onsource-trigger-time", type=float, default=None,
                                help="The onsource trigger time. Pass here to override trigger time in the pe file")
            parser.add_argument("--veto-definer-file", type=str, default=None, help='Pass veto definer file to evaluate acceptable real offsource times.')
            parser.add_argument("--H1-hveto-list", type=str, default=None, help='Pass a HVeto list for H1 to evaluate acceptable real offsource times?')
            parser.add_argument("--L1-hveto-list", type=str, default=None, help='Pass a HVeto list for L1 to evaluate acceptable real offsource times?')
            parser.add_argument("--onsource-reconstruction", default=False, action="store_true", help='Onsource BW reconstruction of GW event candidate for waveform consistency evaluations.')
            parser.add_argument("--offsource-injections", default=False, action="store_true", help='Create n_offsource offsource injections for BW into real (no transient power) data for waveform consistency evaluations.')
            parser.add_argument('--sim-offsource-injections', default=False, action="store_true", help='Create n_offsource simulated (noise) offsource injections for BW for waveform consistency evaluations.')
            parser.add_argument("--onsource-residuals", default=False, action="store_true", help='Onsource residuals run for residuals test of GR.')
            parser.add_argument("--offsource-background", default=False, action="store_true", help='Create n_offsource offsource injections for BW into real (no transient power) data for residuals test of GR.')
            parser.add_argument("--sim-offsource-background", default=False, action="store_true", help='Create n_offsource offsource injections for BW into simulated (no transient power) data for residuals test of GR.')
            parser.add_argument("--osg-deploy", default=False, action="store_true")
            parser.add_argument("--transfer-files", default=True, action="store_true", help='Transfer cache files rather than relying on gwdatafind.')
            parser.add_argument("--offsource-bw-webpage", default=False, action="store_true", help='Generate individual results for offsource BW runs if passed.')
            parser.add_argument("--cbc-fref", default=None, type=float, help='CBC reference BWfrequency.')
            parser.add_argument("--cbc-amporder", default=None, type=float, help='CBC pN amplitude order.')
            parser.add_argument("--cbc-pnorder", default=None, type=float, help='CBC pN phase order.')
            parser.add_argument("--glitch-clean-suffix", type=str, default="_T1700406_v4")
            parser.add_argument("--injection-type", type=str, default=None, help='Select draws (randomly sample PE parameters), maxL (inject Bilby Max Log Likelihood estimate), or MAP (inject Bilby Max a posteriori estimate) for offsource injections.')
            parser.add_argument("--TGR-MDC", default=False, action="store_true", help='Use MDC frames (signals) for onsource residuals.')
            parser.add_argument("--glitch-clean-onsource", default=False, action="store_true", help='Use glitch cleaned frames for onsource runs.')
            parser.add_argument("--noiseless-tgr-mdc", default=False, action="store_true", help='No-noise, onsource residuals-only MDC runs.')
            parser.add_argument("--asd-files", default=None, help="ASD files for simulated onsource residuals runs. Provide ASDs if running in this configuration.", nargs='*')
            # parser.add_argument("--bypass-plots", default=False, action="store_true", help='Do runs only. Generate plots later.')
            parser.add_argument("--offsource-times-path", type=str, default=None, help="Path to XML file containing trigtimes used from old (real data) run. Get from the old run directory.\nNote: in current configuration, this does not check the content of the PE samples it contains.\nDo this manually, ahead of time.")


            ### MARK DEV: cache for MDC (onsource)
        
            opts = parser.parse_args()
            
            ### only options available that make numerous injections into different segments; single injection options reserved for other use-cases. 
            
            if opts.sim_offsource_injections or opts.offsource_injections or opts.offsource_background or opts.sim_offsource_background:
                if opts.sim_offsource_injections and opts.offsource_injections:
                    raise ValueError('You have passed both --offsource-injections and --sim-offsource-injections. Pass one or the other; both not supported for a single run.') 
                
                if opts.offsource_background and opts.sim_offsource_background:
                    raise ValueError('You have passed both --offsource-background and --sim-offsource-background. Pass one or the other; both not supported for a single run.') 
                
                
                if opts.sim_offsource_injections or opts.offsource_injections:
                    if opts.injection_type is None:
                        print('Error: you have not selected an injection type but have passed --sim-offsource-injections or --offsource-injections.')
                        print('Select draws (randomly sample PE parameters), maxL (inject Bilby Max Log Likelihood estimate), or MAP (inject Bilby Max a posteriori estimate) for injection type, and try again.')
                        sys.exit(1)
                    elif opts.injection_type not in ['draws', 'ML', 'MAP']:
                        raise ValueError('Select draws (randomly sample PE parameters), maxL (inject Bilby Max Log Likelihood estimate), or MAP (inject Bilby Max a posteriori estimate) for injection type, and try again.')
            
            if opts.workdir is None:
                print("ERROR: must specify --workdir", file=sys.stderr)
                sys.exit(1)

            if opts.osg_deploy:
                print('WARNING: --osg-deploy option not maintained. Proceed (or edit your own fork) at your own risk.')
                
            # Read PE file
            self.datfile_flag = False
            
            ### pass which OR we are in for bilby
            if opts.bilby_o4a and opts.bilby_o3b:
                raise ValueError('Only O3b or O4a generation Bilby configs supported at this time. Pass --bilby-o4a or --bilby-o3b.')
            
            
            if opts.pesamplesfile:
                if not opts.peconfigfile:
                    print('Error: --pesamplesfile passed but --peconfigfile not. Both are required using individual PE files. Pass both, or pass --pesummaryfile to use that format, and try again.')
                    sys.exit(1)
                elif opts.pesummaryfile:
                    print('You have passed a PE summary file with --pesamplesfile and --peconfigfile. Pass only individual PE files with this flag, and try again.')
                    sys.exit(1) 
                self.datfile_flag = True
            else:
                self.datfile_flag = False


            
            
            ### select which Bilby "generation" we're in, exclusively 
            ### keeping datfile flag to avoid rewrites
            if opts.bilby_o3b:
                self.bilby_o3b_flag = True
                self.bilby_o4a_flag = False

             
            elif opts.bilby_o4a:
                self.bilby_o4a_flag = True
                self.bilby_o3b_flag = False
            
            if self.datfile_flag:
                if not opts.peconfigfile or not opts.pesamplesfile:
                    print("ERROR: Need a valid pe config file, samples file, and a list of PSD files if a pesummaryfile is not passed")
                    sys.exit(1)
                if not opts.psd_files:
                    Warning("Warning: PSD files not passed directly with PE samples file and PE config file. Will try and parse PE config file for them.")
                elif not os.path.isfile(opts.peconfigfile) and not os.path.isfile(opts.pesamplesfile):# and not all([os.path.isfile(x) for x in opts.psd_files]):
                    if not os.path.isfile(opts.peconfigfile):
                        print(f"Config file {opts.peconfigfile} not found")
                    if not os.path.isfile(opts.pesamplesfile):
                        print(f"Samples file {opts.pesamplesfile} not found")         
                    sys.exit(1)
                pe_file = os.path.abspath(opts.pesamplesfile)
                pe_config = configparser.ConfigParser()
                pe_config.optionxform = str
                
                
                
                try:
                    ### try reading (has sections)
                    pe_config.read(opts.peconfigfile)
                except configparser.MissingSectionHeaderError:
                    try:
                        ### if doesn't have sections, do the following:
                        with open(opts.peconfigfile) as stream:
                            ### attach pseduo-section to top of config & read as stream (assume pe config doesn't have sections - need this to read)
                            pe_config.read_string("[top]\n" + stream.read())
                        ### index that pseudo-section as the pe_config
                        pe_config = pe_config['top']
                    except:  
                        print('Error: PE config file not able to be read.')
                        sys.exit(1)
                if opts.psd_files:
                    pe_data = [pe_file, pe_config, opts.psd_files]
                else:
                    pe_data = [pe_file, pe_config, []]

            else:
                if not os.path.isfile(opts.pesummaryfile):
                    print(f"ERROR: pesummary file {opts.pesummaryfile} does not exist",)
                    sys.exit(1)
                ###### READ PE SUMMARY FILE ###### 
                pe_data = read(opts.pesummaryfile)

            ### IWC Config Checking 
            if not os.path.isfile(opts.configfile):
                print(f"ERROR: config file {opts.configfile} does not exist", file=sys.stderr)
                sys.exit(1)
            
            # Read Config File 
            cp = configparser.ConfigParser()
            cp.optionxform = str
            cp.read(opts.configfile)
            return opts, pe_data, cp, self.datfile_flag  
        
        ### Begin setting of Pe variables
        self.opts, self.pe_data, self.iwc_config, self.datfile_flag   = parser()
        
        ################################## Populate some iwc_config options ##################################
        try:
            self.copy_frames = self.iwc_config['condor']['copy-frames']
        except Exception:
            self.iwc_config.set('condor', 'copy-frames', str(False))

        self.workdir = os.path.abspath(self.opts.workdir)
        if os.path.exists(self.workdir):
            print("""
            \nXXX DANGER XXX: path {} already exists.

            Continuing workflow generation will OVERWRITE current workflow files
            (configuration file, injection data, DAGMAN and Bash scripts).  This may
            complicate book-keeping and is not recommended for production analyses.

            """.format(
                self.workdir), file=sys.stderr)

            if not confirm(prompt='Proceed?', resp=False):
                print("You chose wisely, exiting", file=sys.stderr)
                sys.exit(1)

        else:
            print(f"making work-directory: {self.workdir}", file=sys.stdout)
            os.makedirs(self.workdir)

        try:
            self.iwc_config.getboolean('condor', 'osg-deploy')
        except:
            self.iwc_config.set('condor', 'osg-deploy', str(self.opts.osg_deploy))

        try:
            self.iwc_config.get('engine', 'singularity')
        except:
            self.iwc_config.set('engine', 'singularity', str(None))

        if self.iwc_config.get('engine', 'singularity') == 'None':
            print("NOT requiring HAS_SINGULARITY=?=TRUE. Do not need containerization for this run.")
            self.iwc_config.set('engine', 'use-singularity', str(False))
        else:
            self.iwc_config.set('engine', 'use-singularity', str(True))
            self.iwc_config.set('condor', 'transfer-files', str(True))
            print("Requiring HAS_SINGULARITY=?=TRUE")
            print(f"Using image: {self.iwc_config.get('engine', 'singularity')}")
            print("Activating condor file transfers")

        try:
            self.iwc_config.getboolean('condor', 'shared-filesystem')
        except:
            self.iwc_config.set('condor', 'shared-filesystem', str(False))

        if not self.iwc_config.getboolean('condor', 'shared-filesystem'):
            try:
                self.iwc_config.getboolean('condor', 'transfer-files')
            except:
                self.iwc_config.set('condor', 'transfer-files', str(self.opts.transfer_files))
        else:
            self.iwc_config.set('condor', 'transfer-files', str(False))

        try:
            self.dataseed = self.iwc_config.getint('input', 'dataseed')
        except configparser.NoOptionError:
            print("[input] section requires dataseed for sim data", file=sys.stderr)
            print(" (you need this in bayeswave_post, even if real data", file=sys.stderr)
            print(f"...removing {self.workdir}", file=sys.stderr)
            # os.chdir(topdir)
            shutil.rmtree(self.workdir)
            sys.exit(1)
        
        ############################ PE CONFIG PARSING ############################
       
        if self.datfile_flag:
            self.pe_file = self.pe_data[0]
            self.pe_config = self.pe_data[1]
            self.psd_file_names = self.pe_data[2]
            
            if self.bilby_o3b_flag:
                self.pe_ifos = sorted(ast.literal_eval(self.pe_config["analysis"]["ifos"])) ### ### MARK DEV: Read 
            elif self.bilby_o4a_flag:
                self.pe_ifos = sorted(ast.literal_eval(self.pe_config['detectors']))
        
        ### can read hveto list      
                 
        ##################################### GATHER PE OPTIONS #####################################
        ### O3b/O4a conditionals in here
        ### datafile flag only; OR ones done (based entirely on PE summary labels)
        ### put all datafile flag in here 
        
        ### Reading PE summary file
        elif not self.datfile_flag: #self.bilby_o3b_flag: #and not self.datfile_flag:
            
            
            ### o3b unique begin ###
            ### MARK DEV: needed weedout
            if self.bilby_o3b_flag:
            
            ### MARK DEV: 
                if self.opts.pesummarylabel is None:
                    print("PE summary label not found for O3b-type Bilby. Try again with one from this list of available labels: ")
                    print(self.pe_data.labels)
                    sys.exit(1)

                            
                self.pesummarylabel = self.opts.pesummarylabel
                self.PEdir = os.path.join(self.workdir, "PE_dat")
                os.makedirs(self.PEdir)
                self.pe_config = self.pe_data.config[self.pesummarylabel]['config']
                
                self.pe_ifos = sorted(ast.literal_eval(self.pe_config["detectors"]))
                filenames = {self.opts.pesummarylabel: os.path.join(self.PEdir, f"pesummary_{self.pesummarylabel}.dat")}
                self.pe_data.to_dat([self.opts.pesummarylabel], filenames=filenames)
                self.pe_file = filenames[self.opts.pesummarylabel]
                self.pe_config = self.pe_data.config[self.pesummarylabel]['config'] ##### 
        
                ### o3b unique end ###
                ### o4a unique begin ###
            elif self.bilby_o4a_flag:
                
                self.pe_config = self.pe_data.config['config']
              

                self.pe_ifos = sorted([ifo.replace("'", "") for ifo in self.pe_config["detectors"]])

                if self.opts.pesummarylabel:
                    print('Warning: pesummarylabel will not be used for O4a generation Bilby config.')
                
                
                # Bilby style config
                try:
                    ### write PE samples
                    self.PE_dir = os.path.join(self.workdir, "PE_dat") 
                    os.makedirs(self.PE_dir)
                    self.pe_file = os.path.join(self.PE_dir, 'pesummary_samples.dat')
                    df = pd.DataFrame(self.pe_data.samples_dict)
                    df.to_csv(self.pe_file, sep='\t', index=False)
                    
                                
                    # self.bilby_analysis_flag = True
                    
                except:
                    raise ValueError('Valid Bilby PE file(s) not passed to pipeline. Please check your file and try again.')
        
        
        ### CUSTOM OFFSOURCE TRIGTIMES LIST
        self.use_custom_offs_xml = False
        if self.opts.offsource_times_path is not None:
            if not (self.opts.offsource_background or self.opts.sim_offsource_injections or self.opts.offsource_injections):
                print('Error: --offsource-times-path passed without one of:')
                print('--offsource-background')
                print('--sim-offsource-injections')
                print('--offsource-injections')
                print('We only use a --offsource-times-path for an XML loaded with offsource trigtimes if we\'re running on real data. Please correct args and try again.')
                sys.exit(1)
            try:
                with open(self.opts.offsource_times_path) as fp:
                    print(f'Trigtimes file from --offsource-times-path {self.opts.offsource_times_path} file readable.')

            except IOError as err:
                print(f'Error: Trigtimes file from --offsource-times-path not readable or does not exist at {self.opts.offsource_times_path}.')
                sys.exit(1)
            
            try:
                self.sim_insp = io.SimInspiralIO.parseXML(self.opts.offsource_times_path)
            except Exception as err:
                err_mess = str(err)
                if "invalid Column 'process_id' for Table 'sim_inspiral'" in err_mess:
                    try:
                        ### convert old SimInspiral XML table to new format
                        cmd = f'ligolw_no_ilwdchar {self.opts.offsource_times_path}'
                        os.system(cmd)
                        self.sim_insp = io.SimInspiralIO.parseXML(self.opts.offsource_times_path)
                    except:
                        print(f'Error: SimInspiral XML file in old format could not be read-in from {self.opts.offsource_times_path}.')
                        print('Inspect file and try again.')
                        sys.exit(1)
                else:
                    print(f'Error: SimInspiral XML file in old format could not be read-in from {self.opts.offsource_times_path}.')
                    print('Inspect file and try again.')
                    sys.exit(1)
            self.use_custom_offs_xml = True
        
        
        ### Hveto segment list parsing (H1, L1)
        if self.opts.H1_hveto_list is not None:
            ### make abs path
            if not os.path.isabs(self.opts.H1_hveto_list):
                self.opts.H1_hveto_list = os.path.join(os.getcwd(), self.opts.H1_hveto_list)
            ### Check readability of hveto list
            try:
                with open(self.opts.H1_hveto_list) as fp:
                    print(f'H1 hveto list file {self.opts.H1_hveto_list} file readable.')

            except IOError as err:
                print(f'Error: H1 hveto list file not readable or does not exist at {self.opts.H1_hveto_list}.')
                sys.exit(1)

        if self.opts.L1_hveto_list is not None:
            ### make abs path
            if not os.path.isabs(self.opts.L1_hveto_list):
                self.opts.L1_hveto_list = os.path.join(os.getcwd(), self.opts.L1_hveto_list)
            
            ### Check readability of hveto list
            try:
                with open(self.opts.L1_hveto_list) as fp:
                    print(f'L1 hveto list file {self.opts.L1_hveto_list} file readable.')

            except IOError as err:
                print(f'Error: L1 hveto list file not readable or does not exist at {self.opts.L1_hveto_list}.')
                sys.exit(1) 
                
        ### Glitch-Cleaned Frames
        if self.opts.glitch_clean_onsource:
            if self.iwc_config.has_section('onsource_clean_frame'):
                print('Config section onsource_clean_frame exists for glitch-cleaned frames.')
                
                try:
                    self.cleanFrtype = ast.literal_eval(self.iwc_config['onsource_clean_frame']['clean-frtype'])
                except:
                    print('Error: clean-frtype could not be parsed from MDC config section. Check your config and try again if passing --glitch-clean-onsource.')
                    sys.exit(1)
                    
                
                try:
                    self.cleanFrames = ast.literal_eval(self.iwc_config['onsource_clean_frame']['clean-frames'])
                except:
                    self.cleanFrames = None
                    print('Note: --glitch-clean-onsource passed, but clean-frames could not be parsed from MDC config section. Will attempt to get glitch-cleaned frames from gw_data_find.')
                    # sys.exit(1)
                
                ### ifos (keys) are same between clean-frames and clean-channels? 
                if self.cleanFrames is not None:
                    if self.cleanFrames.keys() != self.cleanFrtype.keys():
                        print('Error: Ifo keys for clean-frames and clean-channels are not the same. Please try again.')
                        sys.exit(1)
                        
                    ### Is IFO per clean frame in set of PE Ifos? 
                    for k in self.cleanFrames.keys():
                        if k not in self.pe_ifos:
                            print(f'Error: IFO key {k} not set of IFOs used in PE. Please try again.')
                            sys.exit(1)
                        
                    for ifo, frame in self.cleanFrames.items():
                        ### check accessibility
                        try:
                            with open(frame) as fp:
                                print(f'Glitch-cleaned frame file {frame} readable for {ifo}.')
                            ### make sure readable
                            os.chmod(frame, 0o744)


                        except IOError as err:
                            print(f'Error: Glitch-cleaned frame file {frame} NOT readable for {ifo}.')
                            sys.exit(1)
                    
                        ### clean channel name consistency
                        frame_chan_hold = os.path.basename(frame).split('-')[1] ### channel name in .gwf file (glitch cleaned frame)
                        if frame_chan_hold != self.cleanFrtype[ifo]:
                            print(f'Error: Channel name {frame_chan_hold} in glitch cleaned frame file not equivalent to channel name given in clean-channels for {ifo}: {self.cleanChans[ifo]}')
                            print('Update config and try again.')
                            sys.exit(1)
                         
                
                
                
            else:
                print('Error: Config section onsource_clean_frame does not exist for glitch-cleaned frames. You passed --glitch-clean-onsource. Correct your config and try again.')
                sys.exit(1)

        if self.opts.TGR_MDC:
            if self.iwc_config.has_section('TGR_MDC'):
                print('Config section TGR_MDC exists.')
                # mdcChan = ast.literal_eval(self.iwc_config['TGR_MDC']['mdc-channels'])
                # mdcCache = str(self.iwc_config['TGR_MDC']['mdc-cache'])
                if self.iwc_config.has_option('TGR_MDC', 'mdc-cache'):
                    try:
                        self.mdcCache = str(self.iwc_config['TGR_MDC']['mdc-cache'])
                    except:
                        print('Error: mdc-cache could not be parsed as a string from TGR MDC config section')
                        sys.exit(1)
                else:
                    print('Error: mdc-cache not found in config, with flag --TGR-MDC.')
                    sys.exit(1)
                    
                if self.iwc_config.has_option('TGR_MDC', 'mdc-frtype'):
                    try:
                        self.mdcFrtype = ast.literal_eval(self.iwc_config['TGR_MDC']['mdc-frtype'])
                    except:
                        print('Error: mdc-frtype could not be parsed as a dict from TGR MDC config section')
                        sys.exit(1)
                else:
                    print('Error: mdc-frtype not found in config, with flag --TGR-MDC.')
                    sys.exit(1)
                    
                if self.iwc_config.has_option('TGR_MDC', 'mdc-prefactor'):
                    try:
                        self.mdcPrefactor = int(self.iwc_config['TGR_MDC']['mdc-prefactor'])
                    except:
                        print('Error: mdc-prefactor could not be parsed as an int from TGR MDC config section, with flag --TGR-MDC')
                        sys.exit(1)
                else:
                    print('Error: mdc-prefactor not found in config.')
                    sys.exit(1) 
                
                if self.iwc_config.has_option('TGR_MDC', 'cust_bw_seglen'):
                    try:
                        self.cust_bw_seglen = float(self.iwc_config['TGR_MDC']['cust_bw_seglen'])
                    except:
                        print('Error: custom onsource residuals BW seglen could not be parsed as a float.')
                        sys.exit(1)
                    if self.cust_bw_seglen % 2.0 != 0.0:
                        print('Error: custom onsource residuals BW seglen is not a power of 2. Check your config opts and try again.')
                        sys.exit(1)
                    elif self.cust_bw_seglen > 16.0:
                        print('---------------------------------------------------------------------------------')
                        print('Warning: cust_bw_seglen larger than 16.0 entered. Make sure this was intentional.')
                        print('---------------------------------------------------------------------------------')
                    
                    # elif self.iwc_config.has_option('iwc_options', 'seglen_max'):
                    #     if float(self.iwc_config['iwc_options']['seglen_max']) < self.cust_bw_seglen:
                    #         print('Error: You have entered a cust_bw_seglen greater than seglen_max. Check your configs and try again')
                    #         sys.exit(1)

                
                
                
                ### Test consistency between MDC cache and MDC channels
                with open(self.mdcCache, 'r') as f:
                    lines = f.readlines()

                for l in lines:
                    things = l.split()
                    ifo = f'{things[0]}1'
                    chan = things[1]
                    
                    if self.mdcFrtype[ifo] != chan:
                        print(f'Error: MDC channel {chan} for {ifo} not consistent in mdc-cache file and mdc-channels. Check your configs and MDC.cache file and try again')
                        sys.exit(1) 
            else:
                print('Error: TGR_MDC section not found in config template')        
        else:
            if self.opts.noiseless_tgr_mdc:
                print('Error: --noiseless-tgr-mdc passed but --TGR-MDC not passed. Pass both, and make sure TGR_MDC section of iwc_config is configured properly, and try again.')
                sys.exit(1)
        ############### PSD PARSING ###############
        if self.opts.psd_files is not None and not self.opts.noiseless_tgr_mdc:
            ### MARK DEV: need to be configured in a PE config. 
            print('Using PSD files passed on command line.')
            
            if len(self.pe_ifos) != len(self.psd_file_names):
                print("Error: Number of PSD files supplied needs to be equal to the number of PE ifos") ### OK
                print("Check your PSD files and config file and try again.")
                sys.exit(1)
                # self.bayesline_median_psd_flag = True
                # self.pe_psd_dat = None
            elif len(self.pe_ifos) == len(self.psd_file_names):
                self.pe_psd_dict = {}
                self.pe_psd_dat = {}

                psd_file_sort = sorted(self.psd_file_names)
                
                ### pack PSD files passed standalone into  
                for ind, ifo in enumerate(self.pe_ifos):
                    self.pe_psd_dict[ifo] = psd_file_sort[ind] 

                ### if pe_ifos arent keys of pe_psd_dict
                if sorted(self.pe_ifos) != sorted(self.pe_psd_dict.keys()):
                    print('Error: Mismatch between PSD files passed and PE IFOs used. Check your PSD files and config file and try again.')
                    sys.exit(1)
                    # self.bayesline_median_psd_flag = True
                    # self.pe_psd_dat = None
                else:
                    ### check readability
                    for ifo, psdF in self.pe_psd_dict.items():
                        try:
                            with open(psdF) as fp:
                                print(f'PSD file {psdF} file readable at {ifo}.')
                                fp.close()
                            self.pe_psd_dat[ifo] = np.loadtxt(psdF)
                        except IOError as err:
                            print(f'Error: PSD file not readable or does not exist for {ifo} at {psdF}.')
                            print('Investigate your PSD files and try again.')
                            sys.exit(1)
                           
        elif self.opts.noiseless_tgr_mdc and self.opts.asd_files is not None:     
            self.asd_dict = {}
            self.asd_dat = {}    
            asd_file_sort = sorted(self.opts.asd_files)
            ### pack PSD files passed standalone into  
            for ind, ifo in enumerate(self.pe_ifos):
                self.asd_dict[ifo] = asd_file_sort[ind] 

            ### if pe_ifos arent keys of asd_dict
            if sorted(self.pe_ifos) != sorted(self.asd_dict.keys()):
                print('Error: Mismatch between ASD files passed and PE IFOs used. Check your ASD files and config file and try again.')
                sys.exit(1)
                
            else:
                ### check readability
                for ifo, asdF in self.asd_dict.items():
                    try:
                        with open(asdF) as fp:
                            print(f'ASD file {asdF} file readable at {ifo}.')
                            fp.close()
                        self.asd_dat[ifo] = np.loadtxt(asdF)
                    except IOError as err:
                        print(f'Error: ASD file not readable or does not exist for {ifo} at {asdF}.')
                        print('Investigate your ASD files and try again.')
        elif self.opts.noiseless_tgr_mdc and not self.opts.asd_files:
            print('--asd-files not passed with --noiseless-tgr-mdc. Pass these files, separated by spaces, and try again.')
            sys.exit(1)
   
                    
                        
        ### default to reading psd_dict from str
        elif self.opts.psd_files is None and len(self.pe_ifos) != 0:
            print('PSD files not passed; using PSDs given in PE config file.')
            ### parse PE config PSD dict (or dict_string)
            try:
                pe_psd_dict_hold = self.pe_config['psd-dict']
            except:
                pe_psd_dict_hold = self.pe_config['psd_dict']

            
            ### read PSDs from PE config PSD dict (or dict_string)
            if isinstance(pe_psd_dict_hold, str):
                try:
                    psd_dict = ConfigParse.peDictClean(pe_psd_dict_hold)
                except:
                    psd_dict = ConfigParse.peDictClean(pe_psd_dict_hold)
                    
                self.pe_psd_dict = psd_dict
            elif isinstance(pe_psd_dict_hold, dict):
                self.pe_psd_dict = ast.literal_eval(pe_psd_dict_hold)
            else:
                print('PSD dict not parsable as string or dictionary from PE file.')
            
            ### do we have as many psd's from PE config in from pe psds as PE ifos?
            if len(self.pe_ifos) != len(self.pe_psd_dict.keys()): ##
                print("Number of PSD files supplied needs to be equal to the number of PE ifos. Defaulting to Defaulting to Bayesline PSD computation.") ### OK
                ### MARK DEV: fail here too
                self.bayesline_median_psd_flag = True
                self.pe_psd_dat = None

            ### strip PSD strings of double quotes
            for ifo, psdF in self.pe_psd_dict.items():
                self.pe_psd_dict[ifo] = psdF.replace('""', '')
            
            ### check readability
            self.pe_psd_dat = {}
            for ifo, psdF in self.pe_psd_dict.items():
                try:
                    with open(psdF, 'r') as fp:
                        print(f'PSD file {psdF} file readable for {ifo}, as specified in PE file.')
                        fp.close()
                    self.pe_psd_dat[ifo] = np.loadtxt(psdF)


                except IOError as err:
                    print(f'PSD file not readable or does not exist for {ifo} at {psdF}.')
                    print('\nFull Error Trace:')
                    print(err)
                    sys.exit(1)

                    
        try:
            self.pe_psd_len = int(self.pe_config['psd-length']) 
        except:
            self.pe_psd_len = int(self.pe_config['psd_length']) 
    
        ### chan dict    
        try:
            self.pe_chan_dict = self.pe_config['channel-dict']
        except: 
            self.pe_chan_dict = self.pe_config['channel_dict']

        if isinstance(self.pe_chan_dict, str):
            
            # print('CHAN DICT STRING')
            try:
                self.chan_dict_string = self.pe_config['channel-dict'].strip('{').strip('}').strip().strip(',') ### ### GOOD 
            except:
                self.chan_dict_string = self.pe_config['channel_dict'].strip('{').strip('}').strip().strip(',') ### ### GOOD 
            
            hold_items = self.chan_dict_string.split(',')

            self.pe_channels = {}
            
            # Process each item and add it to the dictionary
            for item in hold_items:
                ifo, chan = item.split(':')
                ifo_corr = ifo.strip().strip("'")
                chan_corr = chan.strip().strip("'")
                if ifo_corr in self.pe_ifos:
                    self.pe_channels[ifo_corr] = f'{ifo_corr}:{chan_corr}'
            # self.pe_chan_upd = {}
            # for ifo in self.pe_channels.keys():
            #     if ifo not in self.pe_ifos:
            #         del(self.pe_channels[ifo])
        
        elif isinstance(self.pe_chan_dict, dict):
            # print('CHAN DICT DICT')
            try:
                self.chan_dict = ast.literal_eval(self.pe_chan_dict)
            except:
                print('Channel dict cannot be parsed as dictionary from config. Check PE file and try again.')
                sys.exit(1)
      
            ### tag this on to filter PE channels not present in PE, as well as tagging on "{ifo}:" to beginning of each. 
            self.pe_channels = {}
            for ifo in self.chan_dict.keys():
                if ifo not in self.pe_ifos:
                    continue
                else:
                    hold_chan = self.chan_dict[ifo].strip("'")
                    self.pe_channels[ifo] = f'{ifo}:{hold_chan}' # self.chan_dict[ifo]# 

            # self.pe_channels = self.chan_dict_dup
        
        self.pe_frames = infer_frame_types(self.iwc_config, self.pe_channels, glitch_clean_suffix=self.opts.glitch_clean_suffix)     
        ############### FLOWS AND FHIGHS ###############
        
        if self.bilby_o3b_flag:

            try:
                self.pe_flows = ast.literal_eval(self.pe_config["minimum-frequency"]) ### ### GOOD, but only one 
            except:
                try:
                    self.pe_flows_string = self.pe_config["minimum-frequency"].strip('{').strip('}').strip().strip(',')
                except:
                    self.pe_flows_string = self.pe_config["minimum_frequency"].strip('{').strip('}').strip().strip(',')
                self.pe_flows = {
                    x.strip(): float(y.strip())
                    for x, y in (
                        element.split(':')
                        for element in self.pe_flows_string.split(',')
                    )
                }

            if type(self.pe_flows) == dict:
                self.pe_flows = [float(self.pe_flows[ifo]) for ifo in self.pe_ifos]
                self.pe_flow = np.max(self.pe_flows)
            elif type(self.pe_flows) in [int, float, str]:
                self.pe_flow = float(self.pe_flows)            
            
            try:
                self.pe_fhighs = ast.literal_eval(self.pe_config["maximum-frequency"])
            except:
                try:
                    self.pe_fhighs = ast.literal_eval(self.pe_config["maximum_frequency"])
                except:
                    self.pe_fhighs_string = self.pe_config["maximum-frequency"].strip('{').strip('}').strip().strip(',')
                    self.pe_fhighs = {
                        x.strip(): float(y.strip())
                        for x, y in (
                            element.split(':')
                            for element in self.pe_fhighs_string.split(',')
                        )
                    }


            if type(self.pe_fhighs) == dict:
                self.pe_fhighs = [float(self.pe_fhighs[ifo]) for ifo in self.pe_ifos]
                self.pe_fhigh = np.min(self.pe_fhighs)
            elif type(self.pe_fhighs) in [int, float, str]:
                self.pe_fhigh = float(self.pe_fhighs)
                
        if self.bilby_o4a_flag:
            try:
                self.pe_flows = ast.literal_eval(self.pe_config["minimum_frequency"]) ### ### GOOD, but only one 

            except:
                try:
                    self.pe_flows_string = self.pe_config["minimum-frequency"].strip('{').strip('}').strip().strip(',')
                except:
                    self.pe_flows_string = self.pe_config["minimum_frequency"].strip('{').strip('}').strip().strip(',')
                self.pe_flows = {
                    x.strip(): float(y.strip())
                    for x, y in (
                        element.split(':')
                        for element in self.pe_flows_string.split(',')
                    )
                }

            if type(self.pe_flows) == dict:
                self.pe_flows = [float(self.pe_flows[ifo]) for ifo in self.pe_ifos]
                self.pe_flow = np.max(self.pe_flows)
            elif type(self.pe_flows) in [int, float, str]:
                self.pe_flow = float(self.pe_flows)
            # try:
            #     self.pe_flow = float(self.pe_config["minimum_frequency"])
            # except:
            #     self.pe_flow = float(self.pe_config["minimum-frequency"])

            
            
            # try rounding pe_fhigh to nearest even num. to prevent radix issue 
            # round up to nearest integer
            try:
                self.pe_fhigh = math.ceil(float(self.pe_config["maximum_frequency"]))
            except:
                self.pe_fhigh = math.ceil(float(self.pe_config["maximum-frequency"]))
            # check to see if even; not: round up
            if self.pe_fhigh % 2 != 0:
                self.pe_fhigh += 1
   
        self.pe_seglen = float(self.pe_config["duration"]) ### ### GOOD
        try:
            self.pe_srate = float(self.pe_config["sampling-frequency"]) ### ### GOOD 
        except:
            self.pe_srate = float(self.pe_config["sampling_frequency"]) ### ### GOOD 
        try:
            self.pe_approx = self.pe_config["waveform-approximant"] ### ### GOOD 
        except:
            self.pe_approx = self.pe_config["waveform_approximant"] ### ### GOOD 
        try:
            self.trigtime = float(self.pe_config["trigger-time"])
        except:
            self.trigtime = float(self.pe_config["trigger_time"])
        
        ################################### pN stuff ###################################:
        if self.opts.cbc_pnorder is not None:
            print("Setting pn order to %d" % self.opts.cbc_pnorder)
            self.pe_pnorder = self.opts.cbc_pnorder
        else:
            try:
                self.pe_pnorder = int(self.pe_config["pn_phase_order"])
                ### self.pe_pnorder = int(self.pe_config["pn_phase_order"])
            except:
                try:
                    self.pe_pnorder = int(self.pe_config["pn-phase-order"])
                except:
                    print("CBC pn cannot be read and has not been supplied via command line.", file=sys.stderr)
                    print("Defaulting to -1 (use highest order available) but recommend checking and retrying from command line.", file=sys.stderr)
                    self.pe_pnorder = -1
        
        if self.opts.cbc_amporder is not None:
            print("Setting amplitude order to %d" % self.opts.cbc_amporder)
            self.pe_amporder = self.opts.cbc_amporder
        else:
            try:
                self.pe_amporder = int(self.pe_config["pn_amplitude_order"]) ### ### GOOD 
                ### self.pe_pnorder = int(self.pe_config["pn_amplitude_order"])

            except:
                try:
                    self.pe_amporder = int(self.pe_config["pn-amplitude-order"]) ### ### GOOD 
                except:
                    print("CBC amporder cannot be read and has not been supplied via command line.", file=sys.stderr)
                    print("Defaulting to 0 but recommend checking and retrying from command line.", file=sys.stderr)
                    self.pe_amporder = 0

        ################################### fref stuff ###################################
        if self.opts.cbc_fref is not None:
            self.pe_fref = self.opts.cbc_fref
        elif self.bilby_o3b_flag:
            try:
                self.pe_fref = float(self.pe_config["reference-frequency"]) ### ### GOOD 
            except:
                print("CBC reference frequency cannot be read and has not been supplied via command line.", file=sys.stderr)
                print("Defaulting to 20 Hz but recommend checking and retrying from command line.", file=sys.stderr)
                self.pe_fref = 20
        elif self.bilby_o4a_flag:
            try:
                self.pe_fref = float(self.pe_config["reference_frequency"]) ### ### GOOD 
            except:
                print("CBC reference frequency cannot be read and has not been supplied via command line.", file=sys.stderr)
                print("Defaulting to 20 Hz but recommend checking and retrying from command line.", file=sys.stderr)
                self.pe_fref = 20
        
        ################################### ADD SUFFIXES FOR XLAL (BW) I/O ###################################
        if self.pe_pnorder in {8, -1}:
            self.pe_approx = f'{self.pe_approx}pseudoFourPN'
        elif self.pe_pnorder == 7:
            self.pe_approx = f'{self.pe_approx}threePointFivePN'
        elif self.pe_pnorder == 6:
            self.pe_approx = f'{self.pe_approx}threePN'
        elif self.pe_pnorder == 5:
            self.pe_approx = f'{self.pe_approx}twoPointFivePN'
        elif self.pe_pnorder == 4:
            self.pe_approx = f'{self.pe_approx}twoPN'
        
        ################################### O4 ###################################


            ### tag this on to filter PE channels not present in PE, as well as tagging on "{ifo}:" to beginning of each. 
 
        ############### END PE ############### 
        
        self.bw_window = float(self.iwc_config["input"]["window"])
        self.bw_dataseed = int(self.iwc_config["input"]["dataseed"])
        self.bw_retries = int(self.iwc_config["condor"]["bw-retries"])
        
        ### mark dev: single Ligo CL utils path, here
        self.ligo_cl_bin = self.iwc_config["engine"]['ligo_clUtil_path']
        # self.gw_data_find = self.iwc_config["engine"]["gw_data_find"]
        self.gw_data_find = os.path.join(self.ligo_cl_bin, 'gw_data_find')
        # self.ligolw_segment_query_dqsegdb = self.iwc_config["engine"]["ligolw_segment_query_dqsegdb"]
        self.ligolw_segment_query_dqsegdb = os.path.join(self.ligo_cl_bin, 'ligolw_segment_query_dqsegdb')
        # self.ligolw_segments_from_cats_dqsegdb = self.iwc_config["engine"]["ligolw_segments_from_cats_dqsegdb"]
        self.ligolw_segments_from_cats_dqsegdb = os.path.join(self.ligo_cl_bin, 'ligolw_segments_from_cats_dqsegdb')
        # self.lwtprint = self.iwc_config["engine"]["lwtprint"]
        self.lwtprint = os.path.join(self.ligo_cl_bin, 'lwtprint')


        if self.opts.veto_definer_file is not None:
            self.veto_definer_abs_path = os.path.abspath(self.opts.veto_definer_file)
            self.veto_definer_file = os.path.basename(self.opts.veto_definer_file)
        self.parent_dir = os.getcwd()
        os.chdir(self.workdir)

        self.bayesline_median_psd_flag = False
        
        ########## Seglen & Srate Max ##########
        try:
            self.seglen_max = float(self.iwc_config["iwc_options"]["seglen_max"])
        except:
            self.seglen_max = 8.0
            self.iwc_config.set("iwc_options", "seglen_max", str(self.seglen_max))

        if self.pe_seglen > self.seglen_max:
            self.pe_seglen = self.seglen_max
            print(f'Warning: pe_seglen of {self.pe_seglen}s greater than seglen_max {self.seglen_max}s.')
            print(f'Setting seglen to {self.pe_seglen}; recomputing PSDs using BayesLine.')
            self.bayesline_median_psd_flag = True

        ### srate max: desired ceiling based on frequency content of some signals (analy_srate = srate_max)
        try:
            self.analy_srate  = float(self.iwc_config["iwc_options"]["srate_max"])
        except:
            # self.analy_srate  = 2048.0 
            # self.iwc_config.set("iwc_options", "srate_max", str(self.analy_srate))
            print('Error: ["iwc_options"]["srate_max"] not found in iwc_config. Pass this in based on the frequency content of your event and try again.')
            print('Tip [Nyquist Theorem]: f_high of signal -> round to nearest power of two -> increment to next power of 2 = srate_necessary')
            sys.exit(1)


        ### in the case that we come down from a higher srate to our desired srate max.
        if self.pe_srate > self.analy_srate:
            # self.analy_srate = srate_max
            ### use analy_srate based on srate_max, and recompute PSD's
            print(f'Warning: setting analysis srate to {self.analy_srate} Hz based on srate_max passed in iwc_config.')
            print(f'PE srate was {self.pe_srate} Hz; recomputing PSDs using BayesLine.') 
            self.bayesline_median_psd_flag = True

        
        # if self.pe_srate < self.srates[next_index]:
        #     print('-----------\nWarning:')
        #     print('PE_srate does not appear to be bounded correctly by Nyquist theorem:')
        #     print(f'PE f_high: {self.pe_fhigh}')
        #     print(f'PE Srate: {self.pe_srate}')
        #     print('-----------')

        self.pe_seg_start = self.trigtime - (self.pe_seglen - 2)  # PE seglens always end 2 seconds after the trigtime


######################################## BayesWave Configuration Constructor Classes ########################################
class BwConfigTemplate(object):
    
    def __init__(self, opts, iwc_config, pe_ifos, pe_seglen, pe_srate, pe_flow, pe_fhigh, pe_fref):
        '''
        High Level BW Config Template: fillable for other analyses 
        '''
        
        ### IMPORTANT: esp. pay attention to setting build for bw in iwc_config
        ### Create class variables
        '''MARK DEV: pass dict for all global params'''
        self.iwc_config = iwc_config
        self.opts = opts
        self.pe_ifos = pe_ifos
        self.pe_seglen = pe_seglen
        self.pe_srate = pe_srate
        self.pe_flow = pe_flow
        self.pe_fhigh = pe_fhigh
        self.pe_fref = pe_fref
        

        # A bayeswave config file template.
        self.bw_config_temp = configparser.ConfigParser()
        self.bw_config_temp.optionxform = str
        self.bw_config_temp.add_section("input")
        self.bw_config_temp.set("input", "dataseed", "1234") ######### check; det sim data; could do looping through this for sim data
        self.bw_config_temp.set("input", "seglen", "") 
        self.bw_config_temp.set("input", "window", "1.0") 
        self.bw_config_temp.set("input", "flow", "20.0") 
        self.bw_config_temp.set("input", "srate", "") 
        self.bw_config_temp.set("input", "PSDlength", "") 
        self.bw_config_temp.set("input", "padding", "0.0") 
        self.bw_config_temp.set("input", "keep-frac", "1.0") 
        self.bw_config_temp.set("input", "rho-threshold", "7.0") 
        self.bw_config_temp.set("input", "ifo-list", "") 
        self.bw_config_temp.add_section("engine") 
        self.bw_config_temp.set("engine", "install_path", "") 
        self.bw_config_temp.set("engine", "bayeswave", "") 
        self.bw_config_temp.set("engine", "bayeswave_post", "") 
        self.bw_config_temp.set("engine", "megaplot", "") 
        self.bw_config_temp.add_section("datafind") 
        self.bw_config_temp.set("datafind", "frtype-list", "") 
        self.bw_config_temp.set("datafind", "channel-list", "") 
        self.bw_config_temp.set("datafind", "url-type", "file")  
        self.bw_config_temp.set("datafind", "veto-categories", "[1]")
        self.bw_config_temp.add_section("bayeswave_options")
        self.bw_config_temp.set("bayeswave_options", "updateGeocenterPSD", "")  
        self.bw_config_temp.set("bayeswave_options", "waveletPrior", "")  
        self.bw_config_temp.set("bayeswave_options", "Dmax", "100")   
        ### set condiitonal here! 
        self.bw_config_temp.set("bayeswave_options", "signalOnly", "")  ######### not seeing in BW pipe 
        self.bw_config_temp.add_section("bayeswave_post_options")  ######### check
        self.bw_config_temp.set("bayeswave_post_options", "0noise", "")  ######### check
        self.bw_config_temp.add_section("condor")  ######### check
        self.bw_config_temp.set("condor", "checkpoint", "")  ######### check
        self.bw_config_temp.set("condor", "bayeswave-request-memory", "")  # config["bayeswave"]["request-memory"]) ??
        self.bw_config_temp.set("condor", "bayeswave-request-disk", "")  # config["bayeswave"]["request-memory"]) ??

        self.bw_config_temp.set("condor", "bayeswave_post-request-memory", "")  # config["bayeswave"]["post-request-memory"]) ?? 
        self.bw_config_temp.set("condor", "bayeswave_post-request-disk", "")  # config["bayeswave"]["post-request-memory"]) ?? 

        self.bw_config_temp.set("condor", "datafind", "/usr/bin/gw_data_find")  ######### check
        self.bw_config_temp.set("condor", "ligolw_print", "/usr/bin/ligolw_print")  ######### check
        self.bw_config_temp.set("condor", "segfind", "/usr/bin/ligolw_segment_query_dqsegdb")  ######### check
        self.bw_config_temp.set("condor", "accounting-group", "")  # config["bayeswave"]["accounting-group"])
        self.bw_config_temp.set("condor", "accounting-group-user", os.popen("whoami").read().strip())
        self.bw_config_temp.add_section("segfind")
        self.bw_config_temp.set("segfind", "segment-url", "https://segments.ligo.org")
        self.bw_config_temp.add_section("segments")
        self.bw_config_temp.set("segments", "h1-analyze", "H1:DMT-ANALYSIS_READY:1")
        self.bw_config_temp.set("segments", "l1-analyze", "L1:DMT-ANALYSIS_READY:1")
        self.bw_config_temp.set("segments", "v1-analyze", "V1:ITF_SCIENCEMODE")
        self.bw_config_temp.add_section("injections")


class IWCBwConfigParent(BwConfigTemplate):
    '''
    IWC BayesWave Config Object: config. options that all of our BW runs (should) have in common
    '''
    def __init__(self, opts, iwc_config, pe_ifos, pe_seglen, srate, pe_flow, pe_fhigh, pe_fref):
        # get params for single class
        super().__init__(opts, iwc_config, pe_ifos, pe_seglen, srate, pe_flow, pe_fhigh, pe_fref)
        # name 
        self.bw_config = self.bw_config_temp
        ### common to all configs
        
        self.bw_config.set("input", "PSDlength", str(pe_seglen))
        self.bw_config.set("input", "srate", str(srate))
        self.bw_config.set("input", "seglen", str(pe_seglen))
        self.bw_config.set("input", "ifo-list", str(pe_ifos))

        ### Join single BW bin path with name of executable
        self.bw_config.set("engine", "bayeswave", os.path.join(iwc_config["engine"]["bw_bin_path"], 'BayesWave'))
        self.bw_config.set("engine", "bayeswave_post", os.path.join(iwc_config["engine"]["bw_bin_path"], 'BayesWavePost'))
        self.bw_config.set("engine", "megaplot", os.path.join(iwc_config["engine"]["bw_bin_path"], 'megaplot.py'))
        
        self.bw_config.set("condor", "universe", iwc_config["condor"]["universe"])
        self.bw_config.set("condor", "bayeswave-request-memory",
                        iwc_config["condor"]["bayeswave-request-memory"])  # config["bayeswave"]["request-memory"])
        self.bw_config.set("condor", "bayeswave_post-request-memory",
                        iwc_config["condor"]["bayeswave_post-request-memory"])
        self.bw_config.set("condor", "bayeswave-request-disk",
                        iwc_config["condor"]["bayeswave-request-disk"])  # config["bayeswave"]["request-memory"])
        self.bw_config.set("condor", "bayeswave_post-request-disk",
                        iwc_config["condor"]["bayeswave_post-request-disk"])
        

        self.bw_config.set("condor", "osg-deploy", iwc_config["condor"]["osg-deploy"])
        self.bw_config.set("engine", "use-singularity", iwc_config["engine"]["use-singularity"])
        self.bw_config.set("condor", "transfer-files", iwc_config["condor"]["transfer-files"])
        self.bw_config.set("condor", "shared-filesystem", iwc_config["condor"]["shared-filesystem"])
        self.bw_config.set("condor", "copy-frames", iwc_config["condor"]["copy-frames"])
        self.bw_config.set("condor", "accounting-group", iwc_config["condor"]["accounting-group"])
        self.bw_config.set("datafind", "sim-data", str(False))


class BwConfigIWCAnaly(IWCBwConfigParent):
    def __init__(self, opts, iwc_config, pe_ifos, pe_seglen, srate, pe_flow, pe_fhigh, pe_fref, analyState=str, analyDat = {'frames':None,'channels':None, 'bayesline_median_psd_flag': False}):
        '''
        IWC BayesWave Config Object: config. options that are distinct across our BW runs.
        '''
        # get params for single class
        super().__init__(opts, iwc_config, pe_ifos, pe_seglen, srate, pe_flow, pe_fhigh, pe_fref)
        # name 
        self.analyDat = analyDat
        self.psd_files = {}
        self.psd_file_names = {}
        
        self.analyState = analyState
        if analyState not in {'onsourceRecon', 'onsourceResid', 'offsourceInj', 'offsourceBacgkr', 'offsourceSimInj'}:
            raise TypeError('Please enter a valid analysis type: onsourceRecon, onsourceResid, offsourceInj, offsourceSimInj or offsourceBacgkr')
        else:
            self.analyState = analyState

        ### make sure we have frame types and channel lists for all other analysis types besides onsourceRecon
        if self.analyState not in {'onsourceRecon', 'offsourceSimInj'}:
            assert(self.analyDat['frames'] is not None and self.analyDat['channels'] is not None)
                 
        if self.analyState == 'onsourceRecon':
            if self.iwc_config.has_option('parallelize_options', 'onsourcereconstructionsthreads'):
                self.ons_recon_threads = int(float(self.iwc_config['parallelize_options']['onsourcereconstructionsthreads']))
                self.ons_recon_chains = 20

                if 20 % self.ons_recon_threads != 0:
                    print('Error: onsourcereconstructionsthreads is not a factor of 20 (default number of chains). Encode 1, 2, 4, or 5 for this value and try again')
                    sys.exit(1)
                elif self.ons_recon_threads == 10 or self.ons_recon_threads == 20:
                    print('Error: onsourcereconstructionsthreads is divisible by 20 (default number of chains) but is too high (10 or 20). Encode 1, 2, 4, or 5 for this value and try again')
                    sys.exit(1)
                else:
                    self.bw_config.set("bayeswave_options", "Nthreads", str(self.ons_recon_threads))
                    self.bw_config.set("bayeswave_options", "Nchain", str(self.ons_recon_chains))

            
            self.frames = self.analyDat['frames'] ### pe_frames
            self.channels = self.analyDat['channels'] ### pe_channels
            self.bayesline_median_psd_flag = analyDat['bayesline_median_psd_flag']
            self.bw_config.set("datafind", "channel-list", str(self.channels))
            self.bw_config.set("datafind", "frtype-list", str(self.frames))
            

                
            ### get rid of; some problem with Torral's changes; also have Nyquist theorem in place for this
            # self.bw_config.set("input", "fhigh", str(pe_fhigh))


            ### this config only used to create BW jobs; not saved
            if self.bayesline_median_psd_flag:
                '''BW CONFIG: ONS. RECON MEDIAN PSD (PE)'''
                self.bl_recon_config = copy.deepcopy(self.bw_config)

                if self.iwc_config.has_option('parallelize_options', 'onsourcereconstructionsthreads'):
                    self.bl_recon_config.set("bayeswave_options", "Nthreads", str(self.ons_recon_threads))
                    self.bl_recon_config.set("bayeswave_options", "Nchain", str(self.ons_recon_chains))
                    

                self.bl_recon_config.set('bayeswave_options', 'bayesLine', '')
                self.bl_recon_config.set('bayeswave_options', 'cleanOnly', '')
                self.bl_recon_config.set('bayeswave_post_options', 'lite', '')

                self.bl_recon_config.remove_option('bayeswave_options', 'signalOnly')
                self.bl_recon_config.remove_option('bayeswave_options', 'glitchOnly')
                self.bl_recon_config.remove_option('bayeswave_options', 'noiseOnly')
                self.bl_recon_config.remove_option('bayeswave_options', 'fullOnly')
                
    
            else: 
                self.bl_recon_config = None
            ### FIXME: relatively pathed
            ### write config back in pipeline; for now; to avoid passing PSD files

        
        elif self.analyState == 'onsourceResid':
            self.frames = self.analyDat['frames']
            self.channels = self.analyDat['channels']
            self.bayesline_median_psd_flag = analyDat['bayesline_median_psd_flag']

            ### unique ### TODO: PAY ATTENTION TO WHERE THESE CONSTRUCTED -> PASS IN 
            self.bw_config.set("datafind", "frtype-list", str(self.frames))
            self.bw_config.set("datafind", "channel-list", str(self.channels))
            ### redundant for residuals
            
            ### do not model PSD if using sim PSD's
            if not opts.noiseless_tgr_mdc:
                self.bw_config.set("bayeswave_options", "bayesLine", "")
                self.bw_config.set("bayeswave_post_options", "bayesLine", "")
            else:
                self.bw_config.remove_option("bayeswave_options", "bayesLine")
                self.bw_config.remove_option("bayeswave_post_options", "bayesLine")
                ### 0 noise realized for those runs
                self.bw_config.set("bayeswave_options", "0noise", "")
                self.bw_config.set("bayeswave_options", "noClean", "")

                
            if self.iwc_config.has_option('parallelize_options', 'onsourceresidualsthreads'):
                self.ons_res_threads = int(float(self.iwc_config['parallelize_options']['onsourceresidualsthreads']))
                self.ons_res_chains = 20

                if 20 % self.ons_res_threads != 0:
                    print('Error: onsourceresidualsthreads is not a factor of 20 (default number of chains). Enter 1, 2, 4, or 5 for this value and try again')
                    sys.exit(1)
                elif self.ons_res_threads == 10 or self.ons_res_threads == 20:
                    print('Error: onsourceresidualsthreads is divisible by 20 (default number of chains) but is too high (10 or 20). Enter 1, 2, 4, or 5 for this value and try again')
                    sys.exit(1)
                else:
                    self.bw_config.set("bayeswave_options", "Nthreads", str(self.ons_res_threads))
                    self.bw_config.set("bayeswave_options", "Nchain", str(self.ons_res_chains))
            
            ### simulated MDC's: PSD file transfers and mdc config options for BW runs
            if not opts.noiseless_tgr_mdc:
                self.bw_config.set('condor', 'extra-files', 'residual_frame')#, extra_files)
            else:
                asdTrans = ','.join(analyDat['use-sim-asds'])
                transString = f'residual_frame,{asdTrans}'
                self.bw_config.set('condor', 'extra-files', transString)
                ### MDC_RES.cache isn't written yet, no read test
                self.bw_config.set('injections', 'mdc-cache', str(self.analyDat['mdc-cache']))
                self.bw_config.set('injections', 'mdc-channels', str(self.analyDat['mdc-channels']))
                self.bw_config.set('injections', 'mdc-prefactor', str(self.analyDat['mdc-prefactor']))
                if 'cust_bw_seglen' in analyDat.keys():
                    self.bw_config.set("input", "seglen", str(self.analyDat['cust_bw_seglen']))
                    self.bw_config.set("input", "window", str(self.analyDat['cust_bw_seglen'] / 2))
                    self.bw_config.set("input", "PSDlength", str(self.analyDat['cust_bw_seglen']))


            ### in current directory
            # with open("config.ini", "w") as f:
            #     self.bw_config.write(f)
            if self.bayesline_median_psd_flag and not opts.noiseless_tgr_mdc:
                self.psd_frames = self.analyDat['psd-frames']
                self.psd_channels = self.analyDat['psd-channels']

                self.bl_resid_config = copy.deepcopy(self.bw_config)
                
                self.bl_resid_config.set("datafind", "channel-list", str(self.psd_channels))
                self.bl_resid_config.set("datafind", "frtype-list", str(self.psd_frames))
                
                if self.iwc_config.has_option('parallelize_options', 'onsourceresidualsthreads'):
                    self.bl_resid_config.set("bayeswave_options", "Nthreads", str(self.ons_res_threads))
                    self.bl_resid_config.set("bayeswave_options", "Nchain", str(self.ons_res_chains))
                    
    
                self.bl_resid_config.set('bayeswave_options', 'bayesLine', '')
                self.bl_resid_config.set('bayeswave_options', 'cleanOnly', '')
                self.bl_resid_config.set('bayeswave_post_options', 'lite', '')

                self.bl_resid_config.remove_option('bayeswave_options', 'signalOnly')
                self.bl_resid_config.remove_option('bayeswave_options', 'glitchOnly')
                self.bl_resid_config.remove_option('bayeswave_options', 'noiseOnly')
                self.bl_resid_config.remove_option('bayeswave_options', 'fullOnly')
                
    
            else: 
                self.bl_resid_config = None

        elif self.analyState == 'offsourceInj':
            ### get rid of; some artifact of Torral's change, maybe; also have Nyquist theorem in place for this
            # self.bw_config.set("input", "fhigh", str(pe_fhigh))
            self.frames = self.analyDat['frames']
            self.channels = self.analyDat['channels']
            
                ### if offsource (configs): 
            if not opts.offsource_bw_webpage:
                self.bw_config.set("bayeswave_post_options", "lite", "")
            ### if injection; only app. here
            self.bw_config.set("bayeswave_options", "inj-fref", str(pe_fref))
            self.bw_config.set("bayeswave_post_options", "inj-fref", str(pe_fref))
            ### unique frame and channel type (same for offsource): 
            self.bw_config.set("datafind", "frtype-list", str(self.frames))
            self.bw_config.set("datafind", "channel-list", str(self.channels))
            ### set extra files
            
            if self.iwc_config.has_option('parallelize_options', 'offsourceinjectionsthreads'):                
                self.offs_inj_threads = int(float(self.iwc_config['parallelize_options']['offsourceinjectionsthreads']))
                self.offs_inj_chains = 20

                if 20 % self.offs_inj_threads != 0:
                    print('Error: offsourceinjectionsthreads is not a factor of 20 (default number of chains). Enter 1, 2, 4, or 5 for this value and try again')
                    sys.exit(1)
                elif self.offs_inj_threads == 10 or self.offs_inj_threads == 20:
                    print('Error: offsourceinjectionsthreads is divisible by 20 (default number of chains) but is too high (10 or 20). Enter 1, 2, 4, or 5 for this value and try again')
                    sys.exit(1)
                else:
                    self.bw_config.set("bayeswave_options", "Nthreads", str(self.offs_inj_threads))
                    self.bw_config.set("bayeswave_options", "Nchain", str(self.offs_inj_chains))
            

            ### UNIQUE: 
            # self.bw_config.add_section("injections")
            ### add all from XML 
            self.bw_config.set("injections", "events", "all")

            ### UNIQUE
            # This is an offsource injection. So BL PSD HAS to be computed.
            # Set BL config object
            self.bl_inj_config = copy.deepcopy(self.bw_config)
            self.bl_inj_config.set('bayeswave_options', 'bayesLine', '')
            self.bl_inj_config.set('bayeswave_options', 'cleanOnly', '')
            self.bl_inj_config.set('bayeswave_post_options', 'lite', '')

            self.bl_inj_config.remove_option('bayeswave_options', 'signalOnly')
            self.bl_inj_config.remove_option('bayeswave_options', 'glitchOnly')
            self.bl_inj_config.remove_option('bayeswave_options', 'noiseOnly')
            self.bl_inj_config.remove_option('bayeswave_options', 'fullOnly')
            
            if self.iwc_config.has_option('parallelize_options', 'offsourceinjectionsthreads'):
                self.bl_inj_config.set('bayeswave_options', 'Nthreads', str(self.offs_inj_threads))
                self.bl_inj_config.set('bayeswave_options', 'Nchain', str(self.offs_inj_chains))

           
            ### fixed: transfer from PSD runs for real offsource injections
            median_psds = \
                ["$(macrooutputDir)_PSDs/post/clean/glitch_median_PSD_forLI_{ifo}.dat".format(ifo=ifo)
                 for ifo in pe_ifos]
            extra_files = ','.join(median_psds)
            self.bw_config.set('condor', 'extra-files', extra_files)
            
            # extra_files = ','.join(median_psds)
            # self.bl_inj_config.set('condor', 'extra-files', extra_files)
            # with open("config.ini", "w") as f:
            #     self.bw_config.write(f)
                
        elif self.analyState == 'offsourceSimInj':

            self.bw_config.set("bayeswave_options", "Dmax", str(100)) ### add param

            self.bw_config.set("bayeswave_options", "inj-fref", str(pe_fref))
            


            self.bw_config.set("bayeswave_post_options", "inj-fref", str(pe_fref))
            if not opts.offsource_bw_webpage:
                self.bw_config.set("bayeswave_post_options", "lite", "")
                
            ### FIXME: can set dicts like this? FIXME: add virgo
            self.bw_config.set("datafind", "frtype-list", str({'H1':'LALSimAdLIGO','L1':'LALSimAdLIGO', 'V1': 'LALSimAdVirgo'}))
            self.bw_config.set("datafind", "channel-list", str({'H1':'LALSimAdLIGO','L1':'LALSimAdLIGO', 'V1': 'LALSimAdVirgo'}))
            self.bw_config.set("datafind", "url-type", 'file') ### FIXME: add to all? 
            # self.bw_config.set("datafind","veto-categories", str([1, 2, 3, 4])) ### not real data; no 
            self.bw_config.set("datafind","sim-data", str(True)) ### stays same, but still put here

            
            if self.iwc_config.has_option('parallelize_options', 'offsourceinjectionsthreads'):                
                self.offs_inj_threads = int(float(self.iwc_config['parallelize_options']['offsourceinjectionsthreads']))
                self.offs_inj_chains = 20

                if 20 % self.offs_inj_threads != 0:
                    print('Error: offsourceinjectionsthreads is not a factor of 20 (default number of chains). Enter 1, 2, 4, or 5 for this value and try again')
                    sys.exit(1)
                elif self.offs_inj_threads == 10 or self.offs_inj_threads == 20:
                    print('Error: offsourceinjectionsthreads is divisible by 20 (default number of chains) but is too high (10 or 20). Enter 1, 2, 4, or 5 for this value and try again')
                    sys.exit(1)
                else:
                    self.bw_config.set("bayeswave_options", "Nthreads", str(self.offs_inj_threads))
                    self.bw_config.set("bayeswave_options", "Nchain", str(self.offs_inj_chains))
                
            ### UNIQUE: 
            # self.bw_config.add_section("injections")
            ### add all from XML 
            self.bw_config.set("injections", "events", "all") ### dummy param; changes per time

  
            # with open("config.ini", "w") as f:
            #     self.bw_config.write(f)
                
                
        elif self.analyState == 'offsourceBacgkr':
            self.frames = self.analyDat['frames']
            self.channels = self.analyDat['channels']
            self.bw_config.set("datafind", "frtype-list", str(self.frames))
            self.bw_config.set("datafind", "channel-list", str(self.channels))
            
            ### if opts.__residuals
            self.bw_config.set("bayeswave_options", "bayesLine", "")
                
            if self.iwc_config.has_option('parallelize_options', 'offsourcebackgroundthreads'):                
                self.offs_backgr_threads = int(float(self.iwc_config['parallelize_options']['offsourcebackgroundthreads']))
                self.offs_backgr_chains = 20

                if 20 % self.offs_backgr_threads != 0:
                    print('Error: offsourcebackgroundthreads is not a factor of 20 (default number of chains). Enter 1, 2, 4, or 5 for this value and try again')
                    sys.exit(1)
                elif self.offs_backgr_threads == 10 or self.offs_backgr_threads == 20:
                    print('Error: offsourcebackgroundthreads is divisible by 20 (default number of chains) but is too high (10 or 20). Enter 1, 2, 4, or 5 for this value and try again')
                    sys.exit(1)
                else:
                    self.bw_config.set("bayeswave_options", "Nthreads", str(self.offs_backgr_threads))
                    self.bw_config.set("bayeswave_options", "Nchain", str(self.offs_backgr_chains))
            
            
            
            self.bw_config.set("bayeswave_post_options", "bayesLine", "")
            ### redundant for offsource
            if not opts.offsource_bw_webpage:
                self.bw_config.set("bayeswave_post_options", "lite", "")
            
            # Hack to make BWP also copy over the copy_moments_files.sh script and the mini
            # output directory where all the moments files are created.
            extra_files = 'copy_moments_files.sh,moments_files'
            self.bw_config.set('condor', 'extra-files', extra_files) ### ADDED IN HERE (so longs as they're created - this is not conditional upon anything in script)
            # with open("config.ini", "w") as f:
            #     self.bw_config.write(f)
    
        elif self.analyState == 'offsourceSimBackgr':

            self.bw_config.set("bayeswave_options", "Dmax", str(100)) ### add param

            # self.bw_config.set("bayeswave_options", "inj-fref", str(pe_fref))

            # self.bw_config.set("bayeswave_post_options", "inj-fref", str(pe_fref))
            if not opts.offsource_bw_webpage:
                self.bw_config.set("bayeswave_post_options", "lite", "")
                
            ### FIXME: can set dicts like this? FIXME: add virgo
            self.bw_config.set("datafind", "frtype-list", str({'H1':'LALSimAdLIGO','L1':'LALSimAdLIGO', 'V1': 'LALSimAdVirgo'})) ###
            self.bw_config.set("datafind", "channel-list", str({'H1':'LALSimAdLIGO','L1':'LALSimAdLIGO', 'V1': 'LALSimAdVirgo'})) ### 
            self.bw_config.set("datafind", "url-type", 'file') ### FIXME: add to all? 
            # self.bw_config.set("datafind","veto-categories", str([1, 2, 3, 4])) ### not real data; no 
            self.bw_config.set("datafind","sim-data", str(True)) ### stays same, but still put here
            
            
            if self.iwc_config.has_option('parallelize_options', 'offsourcebackgroundthreads'):                
                self.offs_backgr_threads = int(float(self.iwc_config['parallelize_options']['offsourcebackgroundthreads']))
                self.offs_backgr_chains = 20

                if 20 % self.offs_backgr_threads != 0:
                    print('Error: offsourcebackgroundthreads is not a factor of 20 (default number of chains). Enter 1, 2, 4, or 5 for this value and try again')
                    sys.exit(1)
                elif self.offs_backgr_threads == 10 or self.offs_backgr_threads == 20:
                    print('Error: offsourcebackgroundthreads is divisible by 20 (default number of chains) but is too high (10 or 20). Enter 1, 2, 4, or 5 for this value and try again')
                    sys.exit(1)
                else:
                    self.bw_config.set("bayeswave_options", "Nthreads", str(self.offs_backgr_threads))
                    self.bw_config.set("bayeswave_options", "Nchain", str(self.offs_backgr_chains))
                
            # ### UNIQUE: 
            # self.bw_config.add_section("injections")
            # ### add all from XML 
            # self.bw_config.set("injections", "events", "all") ### dummy param; changes per time

            extra_files = 'copy_moments_files.sh,moments_files'
            self.bw_config.set('condor', 'extra-files', extra_files)
            
            # with open("config.ini", "w") as f:
            #     self.bw_config.write(f)
    
    
    def get_configs(self):
        
        if self.analyState in ['offsourceBacgkr', 'offsourceSimInj', 'offsourceSimBackgr']:
            return self.bw_config
        elif self.analyState == 'offsourceInj':
            return self.bw_config, self.bl_inj_config
        elif self.analyState == 'onsourceRecon':
            return self.bw_config, self.bl_recon_config
        elif self.analyState == 'onsourceResid':
            return self.bw_config, self.bl_resid_config
